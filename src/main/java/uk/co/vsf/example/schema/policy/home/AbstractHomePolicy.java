package uk.co.vsf.example.schema.policy.home;

import java.util.Calendar;

import uk.co.vsf.example.schema.policy.AbstractPolicy;
import uk.co.vsf.example.schema.risk.RiskAddress;

public abstract class AbstractHomePolicy extends AbstractPolicy implements HomePolicy {

    public AbstractHomePolicy(final RiskAddress risk, final Calendar startDate, final Calendar expiryDate) {
        super(risk, startDate, expiryDate);
    }

}
