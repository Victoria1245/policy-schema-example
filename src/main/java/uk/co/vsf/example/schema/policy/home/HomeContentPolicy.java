package uk.co.vsf.example.schema.policy.home;

import java.math.BigDecimal;

public interface HomeContentPolicy extends HomePolicy {

    public BigDecimal getSumInsuredValue();
}
