package uk.co.vsf.example.schema.policy.home;

import java.math.BigDecimal;

public interface HomeBuildingPolicy extends HomePolicy {

    public BigDecimal getRebuildValue();
}
