package uk.co.vsf.example.schema.policy;

import java.util.Calendar;

import uk.co.vsf.example.schema.risk.Risk;

public abstract class AbstractPolicy implements Policy {

    private final Calendar startDate;
    private final Calendar expiryDate;
    private final String policyStatus;
    private final Risk risk;

    protected AbstractPolicy(final Risk risk, final Calendar startDate, final Calendar expiryDate) {
        this.risk = risk;
        this.startDate = startDate;
        this.expiryDate = expiryDate;
        this.policyStatus = "ACTIVE";
    }

    public void cancel() {
        // TODO Auto-generated method stub

    }

    public Calendar getStartDate() {
        return startDate;
    }

    public Calendar getExpiryDate() {
        return expiryDate;
    }

    public String getPolicyStatus() {
        return policyStatus;
    }

    public Risk getRisk() {
        return risk;
    }
}
