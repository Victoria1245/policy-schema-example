package uk.co.vsf.example.schema.policy.motor;

import java.util.Calendar;

import uk.co.vsf.example.schema.risk.RiskVehicle;

public class PrivateCarPolicy extends AbstractMotorPolicy {

    public PrivateCarPolicy(final RiskVehicle risk, final Calendar startDate, final Calendar expiryDate) {
        super(risk, startDate, expiryDate);
    }

}
