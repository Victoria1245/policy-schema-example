package uk.co.vsf.example.schema.policy.home;

import java.math.BigDecimal;
import java.util.Calendar;

import uk.co.vsf.example.schema.risk.RiskAddress;

public class ContentPolicy extends AbstractHomePolicy implements HomeContentPolicy {

    public BigDecimal sumInsuredValue;

    public ContentPolicy(final RiskAddress risk,
            final Calendar startDate,
            final Calendar expiryDate,
            final BigDecimal sumInsuredValue) {
        super(risk, startDate, expiryDate);
        this.sumInsuredValue = sumInsuredValue;
    }

    public BigDecimal getSumInsuredValue() {
        return sumInsuredValue;
    }

}
