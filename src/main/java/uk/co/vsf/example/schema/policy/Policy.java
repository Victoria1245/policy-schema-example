package uk.co.vsf.example.schema.policy;

import java.util.Calendar;

import uk.co.vsf.example.schema.risk.Risk;

public interface Policy {

    public void cancel();

    public Calendar getStartDate();

    public Calendar getExpiryDate();

    public String getPolicyStatus();

    public Risk getRisk();
}
