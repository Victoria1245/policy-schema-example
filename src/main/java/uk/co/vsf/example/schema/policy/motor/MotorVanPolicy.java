package uk.co.vsf.example.schema.policy.motor;

import java.math.BigDecimal;

public interface MotorVanPolicy extends MotorPolicy {

    public BigDecimal getToolsSumInsuredValue();
}
