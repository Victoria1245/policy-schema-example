package uk.co.vsf.example.schema.policy.home;

import java.math.BigDecimal;
import java.util.Calendar;

import uk.co.vsf.example.schema.risk.RiskAddress;

public class BuildingPolicy extends AbstractHomePolicy implements HomeBuildingPolicy {

    private final BigDecimal rebuildValue;

    public BuildingPolicy(final RiskAddress risk,
            final Calendar startDate,
            final Calendar expiryDate,
            final BigDecimal rebuildValue) {
        super(risk, startDate, expiryDate);
        this.rebuildValue = rebuildValue;
    }

    public BigDecimal getRebuildValue() {
        return rebuildValue;
    }

}
