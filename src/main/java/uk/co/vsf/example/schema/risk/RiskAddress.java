package uk.co.vsf.example.schema.risk;

public interface RiskAddress extends Risk{

    public String getLine1();

    public String getLine2();

    public String getLine3();

    public String getLine4();

    public String getLine5();

    public String getPostCode();
}
