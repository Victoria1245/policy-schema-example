package uk.co.vsf.example.schema.policy.motor;

import java.math.BigDecimal;
import java.util.Calendar;

import uk.co.vsf.example.schema.risk.RiskVehicle;

public class VanPolicy extends AbstractMotorPolicy implements MotorVanPolicy {

    private final BigDecimal toolsSumInsuredValue;

    public VanPolicy(final RiskVehicle risk,
            final Calendar startDate,
            final Calendar expiryDate,
            final BigDecimal toolsSumInsuredValue) {
        super(risk, startDate, expiryDate);
        this.toolsSumInsuredValue = toolsSumInsuredValue;
    }

    public BigDecimal getToolsSumInsuredValue() {
        return toolsSumInsuredValue;
    }

}
