package uk.co.vsf.example.schema.policy.home;

import java.math.BigDecimal;
import java.util.Calendar;

import uk.co.vsf.example.schema.risk.RiskAddress;

/**
 * Disadvantage to this particular class hierarchy is that with Java we only
 * have single extension, so the home buildings and content has to be modelled
 * independently. Alternative is to use composition and model it as HomePolicy
 * containing buildings and/or content.
 * 
 * @author Victoria
 */
public class BuildingContentPolicy extends AbstractHomePolicy implements HomeBuildingPolicy, HomeContentPolicy {

    private final BigDecimal rebuildValue;
    private final BigDecimal sumInsuredValue;

    public BuildingContentPolicy(final RiskAddress risk,
            final Calendar startDate,
            final Calendar expiryDate,
            final BigDecimal rebuildValue,
            final BigDecimal sumInsuredValue) {
        super(risk, startDate, expiryDate);
        this.rebuildValue = rebuildValue;
        this.sumInsuredValue = sumInsuredValue;
    }

    public BigDecimal getRebuildValue() {
        return rebuildValue;
    }

    public BigDecimal getSumInsuredValue() {
        return sumInsuredValue;
    }

}
