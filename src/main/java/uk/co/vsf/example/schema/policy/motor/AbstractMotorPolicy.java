package uk.co.vsf.example.schema.policy.motor;

import java.util.Calendar;

import uk.co.vsf.example.schema.policy.AbstractPolicy;
import uk.co.vsf.example.schema.risk.Risk;

public abstract class AbstractMotorPolicy extends AbstractPolicy implements MotorPolicy {

    public AbstractMotorPolicy(final Risk risk, final Calendar startDate, final Calendar expiryDate) {
        super(risk, startDate, expiryDate);
    }
}
